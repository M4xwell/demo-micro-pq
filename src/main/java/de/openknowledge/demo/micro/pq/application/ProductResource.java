package de.openknowledge.demo.micro.pq.application;

import de.openknowledge.demo.micro.pq.domain.Product;
import de.openknowledge.demo.micro.pq.domain.ProductBuilder;
import de.openknowledge.demo.micro.pq.domain.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.apache.commons.lang3.Validate.notNull;

/**
 *
 */
@Path("/product")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {

  private static final Logger LOG = LoggerFactory.getLogger(ProductResource.class);

  @Inject
  private ProductRepository productRepository;

  @GET
  @Path("{id}")
  public Response getProduct(@PathParam("id") final Long id) {
    notNull(id, "id should not be null");
    LOG.info("Get product with id: {}", id);

    try {
      Product product = this.productRepository.findById(id);
      return Response
          .status(Response.Status.OK)
          .entity(product)
          .build();
    } catch (IllegalArgumentException i) {
      LOG.warn("Could not get product with id: {}", id);
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    }
  }

  @GET
  public Response getAllProduct() {
    LOG.info("Get all products");

    List<Product> products = this.productRepository.findAll();

    return Response
        .status(Response.Status.OK)
        .entity(products)
        .build();
  }

  @POST
  public Response createProduct(final BaseProductDTO productDTO) {
    LOG.debug("Create product: {}", productDTO.toString());

    Product product = new ProductBuilder()
        .setName(productDTO.getName())
        .setPrize(productDTO.getPrize())
        .createProduct();

    this.productRepository.create(product);
    LOG.info("Created new product with id: {}", product.getId());

    return Response
        .status(Response.Status.NO_CONTENT)
        .build();
  }

  @DELETE
  @Path("{id}")
  public Response removeProduct(@PathParam("id") final Long id) {
    notNull(id, "id should not be null");
    LOG.info("Remove product with id: {}", id);

    try {
      this.productRepository.delete(id);

      return Response
          .status(Response.Status.NO_CONTENT)
          .build();
    } catch (IllegalArgumentException i) {
      LOG.warn("Could not delete product with id: {}", id);
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    }
  }
}
