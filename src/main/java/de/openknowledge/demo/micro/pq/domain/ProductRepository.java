package de.openknowledge.demo.micro.pq.domain;

import de.openknowledge.demo.micro.pq.infrastructure.stereotype.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

import static org.apache.commons.lang3.Validate.notNull;

@Repository
public class ProductRepository {

  private static final Logger LOG = LoggerFactory.getLogger(ProductRepository.class);

  @PersistenceContext(name = "jpa-unit")
  private EntityManager entityManager;

  public Product findById(final Long id) {
    notNull(id, "id should not be null");
    LOG.debug("Get product by id: {}", id);

    Product product = this.entityManager.find(Product.class, id);

    if (product == null) {
      throw new IllegalArgumentException("Product does not exist with id: " + id);
    }

    return product;
  }

  public List<Product> findAll() {
    LOG.debug("Get all products");

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Product> cq = cb.createQuery(Product.class);

    Root<Product> root = cq.from(Product.class);
    cq.select(root);

    TypedQuery<Product> query = this.entityManager.createQuery(cq);

    return query.getResultList();
  }

  @Transactional
  public Product create(final Product product) {
    notNull(product, "product must not be null");
    LOG.debug("Create product {}", product.toString());

    this.entityManager.persist(product);
    return product;
  }

  @Transactional
  public void delete(final Long id) {
    notNull(id, "id should not be null");
    LOG.debug("Delete product with id: {}", id);

    Product product = this.entityManager.find(Product.class, id);

    if (product == null) {
      throw new IllegalArgumentException("Product does not exist with id: " + id);
    }

    this.entityManager.remove(product);
  }
}
