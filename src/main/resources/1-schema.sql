CREATE TABLE public.TAB_PRODUCT
(
    col_id    SERIAL NOT NULL,
    col_name  VARCHAR(255),
    col_price BIGINT,
    PRIMARY KEY (col_id)
);
