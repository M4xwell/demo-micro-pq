package de.openknowledge.demo.micro.pq.domain;

public class ProductBuilder {
  private Long id;

  private String name;

  private long prize;

  public ProductBuilder setId(final Long id) {
    this.id = id;
    return this;
  }

  public ProductBuilder setName(final String name) {
    this.name = name;
    return this;
  }

  public ProductBuilder setPrize(final long prize) {
    this.prize = prize;
    return this;
  }

  public Product createProduct() {
    return new Product(id, name, prize);
  }
}
